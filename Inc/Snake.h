#ifndef KP_SNAKE_H
#define KP_SNAKE_H 100
#endif

/*
 * Includes
 */

#include <stdio.h>
#include "stm32f429i_discovery_lcd.h"

/* Dołaczenie definicji funkcji komunikujacych sie z ukladem STMPE811 odpowiadajacym za odczyt miejsca dotyku na wyswietlaczu lcd. */
#include "i2c_stm32_my.h"
/* Dołaczenie definicji funkcji obsługujacych odczyt pozycji dotyku na lcd. */
#include "LCD_touch.h"
#include <math.h>
#include <stdbool.h>
#include <stdlib.h>
#include <time.h>

#define AUTHOR	"Kinga Pruban"
#define GAME	"SNAKE"
#define VERSION	"1.0.0"

/*
 * Plansza po której porusza się snake
 */
#define BOARD_LEFT_X 			30
#define BOARD_LEFT_Y			40
#define BOARD_RIGHT_X 			200
#define BOARD_RIGHT_Y			280
#define BOARD_BCK_COLOR			LCD_COLOR_DARKGREEN
/*
 * Ramka planszy
 */
#define BORDER_COLOR			LCD_COLOR_GREEN
#define BORDER_SIZE				5
#define BORDER_LENGTH_X			BOARD_RIGHT_X - 10
#define BORDER_LENGTH_Y			BOARD_RIGHT_Y - 20
#define BORDER_X				BOARD_LEFT_X - BORDER_SIZE
#define BORDER_Y				BOARD_LEFT_Y - BORDER_SIZE
#define BORDER_X_RIGHT			BOARD_RIGHT_X + 10
#define BORDER_Y_DOWN			BOARD_RIGHT_Y + 10

/*
 * Ustawienia gry
 */
#define GAME_LIFE_MAX		3
#define GAME_SPEED_MAX 		250	// 250 ms
#define GAME_SPEED_DEFAULT	500	// 500 ms
/*
 * Umiejscowienie lini tekstowych i koloru
 */
#define GAME_TEXT_LINE_1_X		20				// linia z punktami X
#define GAME_TEXT_LINE_1_Y		10				// linia z punktami Y
#define GAME_TEXT_POINTS_X		100				// pozycja wyświetlenia wartości punktów na 1 lini
#define GAME_TEXT_LIFE_X_1		160				// pozycja wyświetlenia życia 1
#define GAME_TEXT_LIFE_X_2		175				// pozycja wyświetlenia życia 2
#define GAME_TEXT_LIFE_X_3		190				// pozycja wyświetlenia życia 3
#define GAME_TEXT_LINE_2_Y		150				// linia end info 1
#define GEME_TEXT_LINE_3_Y		180				// linia end info 2
#define GAME_TEXT_INFO_COLOR	LCD_COLOR_GREEN // kolor tekstu

#define GAME_MENU_BUTTON_WIDTH	140
#define GAME_MENU_BUTTON_HEIGHT	50
#define GAME_MENU_BUTTON_X		50

#define GAME_MENU_BUTTON_1		1
#define GAME_MENU_BUTTON_1_Y	40
#define GAME_MENU_LINE_1_Y		GAME_MENU_BUTTON_1_Y + 20

#define GAME_MENU_BUTTON_2		2
#define GAME_MENU_BUTTON_2_Y	GAME_MENU_BUTTON_HEIGHT * 2 + GAME_MENU_BUTTON_1_Y
#define GAME_MENU_LINE_2_Y		GAME_MENU_BUTTON_2_Y + 20

#define GAME_MENU_BUTTON_3		3
#define GAME_MENU_BUTTON_3_Y	GAME_MENU_BUTTON_HEIGHT * 2 + GAME_MENU_BUTTON_2_Y
#define GAME_MENU_LINE_3_Y		GAME_MENU_BUTTON_3_Y + 20

/*
 * Wymiary i kolor Snake'a i celu
 */
#define SNAKE_MAX_SIZE		306				//
#define SNAKE_ELEMENTR_SIZE	10				// rozmiar elementu gry
#define SNAKE_COLOR_PIXEL	LCD_COLOR_BLUE	// kolor elemntow snake'a
#define SNAKE_COLOR_APPLE	LCD_COLOR_RED	// kolor celu

/*
 * Kierunek Snake'a
 */
#define SNAKE_DIRECTION_LEFT	0
#define SNAKE_DIRECTION_RIGHT	1
#define SNAKE_DIRECTION_UP		2
#define SNAKE_DIRECTION_DOWN	3

/*
 * Struktura opisująca Snake'a
 */
struct KP_Snake
{
	uint16_t Snake[SNAKE_MAX_SIZE][2];	// tablica opisująca Snake'a koordynatami (x,y)
	uint16_t Length;					// długość Snake'a
	uint16_t LastIndex;					// index ogona
	uint16_t Direction;					// kierunek poruszania się
	uint16_t Points;					// liczba punktów
};

void KP_Snake_Start();

