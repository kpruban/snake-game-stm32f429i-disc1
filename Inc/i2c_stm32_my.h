 /****************************************************************************
 * i2c_stm32_my.h
 *
 * Plik zawierajacy definicje funkcji sluzacych do komunikacji poprzez i2c
 * 
 * 19-05-2018r.
 *
 ****************************************************************************/
#ifndef i2c_stm32_my_h
#define i2c_stm32_my_h


#include <stm32f4xx.h>
#include <inttypes.h>
 
/**
 * @brief  Initializes I2C
 * @param  None
 * @retval None
 */
void I2C_Init (void);

/**
 * @brief  Reads single byte from slave 
 * @param  address: 7 bit slave address, left aligned, bits 7:1 are used, LSB bit is not used
 * @param  reg: register to read from
 * @retval Data from slave
 */
 uint8_t I2C_Read(uint8_t address, uint8_t reg); 

/**
 * @brief  Reads multi bytes from slave 
 * @param  uint8_t address: 7 bit slave address, left aligned, bits 7:1 are used, LSB bit is not used
 * @param  uint8_t reg: register to read from
 * @param  uint8_t *data: pointer to data array to store data from slave
 * @param  uint8_t count: how many bytes will be read
 * @retval None
 */
void I2C_ReadMulti( uint8_t address, uint8_t reg, uint8_t* data, uint16_t count);
 
/**
 * @brief  Writes single byte to slave
 * @param  *I2Cx: I2C used
 * @param  address: 7 bit slave address, left aligned, bits 7:1 are used, LSB bit is not used
 * @param  reg: register to write to
 * @param  data: data to be written
 * @retval None
 */
void I2C_Write(  uint8_t address,  uint8_t reg, uint8_t data);  
 
/**
 * @brief  I2C Start condition
 * @param  *I2Cx: I2C used
 * @param  address: slave address
 * @param  direction: master to slave or slave to master
 * @param  ack: ack enabled or disabled
 * @retval Start condition status
 * @note   For private use
 */
int16_t I2C_Start(uint8_t adres, uint8_t tryb_pracy, uint8_t bit_ack);

/**
 * @brief  Stop condition on I2C
 * @param  None
 * @retval Stop condition status
 * @note   For private use
 */
uint8_t I2C_Stop(void);

/**
 * @brief  Reads byte without ack
 * @param  None
 * @retval Byte from slave
 * @note   For private use
 */
uint8_t I2C_ReadNack(void);


/**
 * @brief  Reads byte with ack
 * @param  None
 * @retval Byte from slave
 * @note   For private use
 */
uint8_t I2C_ReadAck(void);

/**
 * @brief  Writes to slave
 * @param  data: data to be sent
 * @retval None
 * @note   For private use
 */
void 		I2C_WriteData(uint8_t data);
 
#endif
  
