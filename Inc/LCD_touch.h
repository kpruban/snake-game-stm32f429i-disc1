 /****************************************************************************
 * LCD_touch.h
 *
 * Plik zawierajacy definicje funkcji sluzacych do obslugi wyswietlacza
 * dotykowego  
 * 
 * 19-05-2018r.
 *
 ****************************************************************************/
 
         //								 (240, 0)
				//(0, 0)   _ _ _ _ (x1,y1)  ---> x
				//		|		|				|
				// 		|		|				|
				//		|		|				|
				//	Y	V		|_ _ _ _|
				//		(x0,y0)					
				//		(0, 320)			(240, 320)
 
 
#ifndef LCD_touch_h
#define LCD_touch_h

#include <inttypes.h>

#define STMPE811_ADDRESS				0x82

/* STMPE811 Chip ID on reset */
#define STMPE811_CHIP_ID_VALUE			0x0811	//Chip ID

/* Registers */
#define STMPE811_CHIP_ID				0x00	//STMPE811 Device identification
#define STMPE811_ID_VER					0x02	//STMPE811 Revision number; 0x01 for engineering sample; 0x03 for final silicon
#define STMPE811_SYS_CTRL1				0x03	//Reset control
#define STMPE811_SYS_CTRL2				0x04	//Clock control
#define STMPE811_SPI_CFG				0x08	//SPI interface configuration
#define STMPE811_INT_CTRL				0x09	//Interrupt control register
#define STMPE811_INT_EN					0x0A	//Interrupt enable register
#define STMPE811_INT_STA				0x0B	//Interrupt status register
#define STMPE811_GPIO_EN				0x0C	//GPIO interrupt enable register
#define STMPE811_GPIO_INT_STA			0x0D	//GPIO interrupt status register
#define STMPE811_ADC_INT_EN				0x0E	//ADC interrupt enable register
#define STMPE811_ADC_INT_STA			0x0F	//ADC interface status register
#define STMPE811_GPIO_SET_PIN			0x10	//GPIO set pin register
#define STMPE811_GPIO_CLR_PIN			0x11	//GPIO clear pin register
#define STMPE811_MP_STA					0x12	//GPIO monitor pin state register
#define STMPE811_GPIO_DIR				0x13	//GPIO direction register
#define STMPE811_GPIO_ED				0x14	//GPIO edge detect register
#define STMPE811_GPIO_RE				0x15	//GPIO rising edge register
#define STMPE811_GPIO_FE				0x16	//GPIO falling edge register
#define STMPE811_GPIO_AF				0x17	//alternate function register
#define STMPE811_ADC_CTRL1				0x20	//ADC control
#define STMPE811_ADC_CTRL2				0x21	//ADC control
#define STMPE811_ADC_CAPT				0x22	//To initiate ADC data acquisition
#define STMPE811_ADC_DATA_CHO			0x30	//ADC channel 0
#define STMPE811_ADC_DATA_CH1			0x32	//ADC channel 1
#define STMPE811_ADC_DATA_CH2			0x34	//ADC channel 2
#define STMPE811_ADC_DATA_CH3			0x36	//ADC channel 3
#define STMPE811_ADC_DATA_CH4			0x38	//ADC channel 4
#define STMPE811_ADC_DATA_CH5			0x3A	//ADC channel 5
#define STMPE811_ADC_DATA_CH6			0x3C	//ADC channel 6
#define STMPE811_ADC_DATA_CH7			0x3E	//ADC channel 7
#define STMPE811_TSC_CTRL				0x40	//4-wire touchscreen controller setup
#define STMPE811_TSC_CFG				0x41	//Touchscreen controller configuration
#define STMPE811_WDW_TR_X				0x42	//Window setup for top right X
#define STMPE811_WDW_TR_Y				0x44	//Window setup for top right Y
#define STMPE811_WDW_BL_X				0x46	//Window setup for bottom left X
#define STMPE811_WDW_BL_Y				0x48	//Window setup for bottom left Y
#define STMPE811_FIFO_TH				0x4A	//FIFO level to generate interrupt
#define STMPE811_FIFO_STA				0x4B	//Current status of FIFO
#define STMPE811_FIFO_SIZE				0x4C	//Current filled level of FIFO
#define STMPE811_TSC_DATA_X				0x4D	//Data port for touchscreen controller data access
#define STMPE811_TSC_DATA_Y				0x4F	//Data port for touchscreen controller data access
#define STMPE811_TSC_DATA_Z				0x51	//Data port for touchscreen controller data access
#define STMPE811_TSC_DATA_XYZ			0x52	//Data port for touchscreen controller data access
#define STMPE811_TSC_FRACTION_Z			0x56	//Touchscreen controller FRACTION_Z
#define STMPE811_TSC_DATA				0x57	//Data port for touchscreen controller data access
#define STMPE811_TSC_I_DRIVE			0x58	//Touchscreen controller drivel
#define STMPE811_TSC_SHIELD				0x59	//Touchscreen controller shield
#define STMPE811_TEMP_CTRL				0x60	//Temperature sensor setup
#define STMPE811_TEMP_DATA				0x61	//Temperature data access port
#define STMPE811_TEMP_TH				0x62	//Threshold for temperature controlled interrupt

  
/**
 * @brief  Enum for set how to read x and y from controller
 * @note   You may need experimenting to get proper orientation to match your LCD
 */
typedef enum {
	TM_STMPE811_Orientation_Portrait_1,  /*!< Portrait orientation mode 1 */
	TM_STMPE811_Orientation_Portrait_2,  /*!< Portrait orientation mode 2 */
	TM_STMPE811_Orientation_Landscape_1, /*!< Landscape orientation mode 1 */
	TM_STMPE811_Orientation_Landscape_2, /*!< Landscape orientation mode 2 */
} TM_STMPE811_Orientation_t;

/**
 * @brief  Enumeration for touch pressed or released
 */
typedef enum {
	TM_STMPE811_State_Pressed,  /*!< Touch detected as pressed */
	TM_STMPE811_State_Released, /*!< Touch detected as released/not pressed */
	TM_STMPE811_State_Ok,       /*!< Result OK. Used on initialization */
	TM_STMPE811_State_Error     /*!< Result error. Used on initialization */
} TM_STMPE811_State_t;

/**
 * @brief  Main structure, which is passed into @ref TM_STMPE811_ReadTouch function
 */
typedef struct {
	uint16_t x;                            /*!< X coordinate on LCD for touch */ 
	uint16_t y;                            /*!< Y coordinate on LCD for touch */
	TM_STMPE811_State_t pressed;           /*!< Pressed touch status */
	TM_STMPE811_State_t last_pressed;      /*!< Last pressed touch status */
	TM_STMPE811_Orientation_t orientation; /*!< Touch screen orientation to match your LCD orientation */
} TM_STMPE811_t;

/* Backward compatibility */
typedef TM_STMPE811_t STMPE811_TouchData;


/**
 * @defgroup TM_STMPE811_Functions
 * @brief    Library Functions
 * @{
 */
 
/**
 * @brief  Initializes STMPE811 Touch driver
 * @param  None
 * @retval Member of @ref TM_STMPE811_State_t
 */
TM_STMPE811_State_t STMPE811_Init(void); 

/**
 * @brief  Reads touch coordinates
 * @param  *structdata: Pointer to @ref TM_STMPE811_t to store data into
 * @retval Touch status:
 *            - TM_STMPE811_State_Pressed: Touch detected as pressed, coordinates valid
 *            - TM_STMPE811_State_Released: Touch detected as not pressed, coordinates not valid
 */
TM_STMPE811_State_t TM_STMPE811_ReadTouch(STMPE811_TouchData *structdata);
/**/

/**
 * @brief  Checks if touch data is inside specific rectangle coordinates
 * @param  sd: Pointer to @ref TM_STMPE811_t to get data from
 * @param  xPos: Top-left X position of rectangle
 * @param  yPos: Top-left Y position of rectangle
 * @param  w: Rectangle width
 * @param  h: Rectangle height:
 * @retval Touch inside rectangle status:
 *            - 0: Touch is outside rectangle
 *            - > 0: Touch is inside rectangle
 * @note   Defined as macro for faster execution
 */ 
#define TM_STMPE811_TouchInRectangle(sd, xPos, yPos, w, h)	(((sd)->x >= (xPos)) && ((sd)->x < (xPos + w)) && ((sd)->y >= (yPos)) && ((sd)->y < (yPos + h)))



#endif

