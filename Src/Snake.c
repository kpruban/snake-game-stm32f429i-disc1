/*
 * Snake.c
 *
 *  Created on: 26 gru 2023
 *      Author: Pruban Kinga
 */
#include "Snake.h"

STMPE811_TouchData LcdTouchData;
uint16_t TouchScreenCoordinates[2];
uint32_t DelayTime;
volatile uint8_t ScreenTouched = 0, GameOver = 0;
int8_t Lifes = GAME_LIFE_MAX;
uint16_t Snake_Head[2], Snake_Head_Last[2], Snake_Foot[2], Snake_Food[2];
struct KP_Snake snake;

void InitScreen();
void RunNewGame();
void DrawGameMenu();
void DrawGameBoard();
void DrawControlInfo();
void DrawGameInfo();
void DrawGamePoints(uint16_t points, uint value);
void DrawElement(uint16_t x, uint16_t y, uint value);
void DrawHeart(uint16_t x, uint16_t y);
void DrawEndInfo(char* text1, char* text2, uint value);
void SetupDefaultSnake();
void AddElementToSnake(uint16_t* array);
void DeleteElementFromSnake(uint16_t index, uint16_t* array);
void GenerateNewTarget();
void GenerateRandomCoordinates(uint16_t* array);
uint8_t MatchesSnakeLocations(uint16_t* array);
uint16_t CheckDirection(uint16_t* array);
uint16_t CheckButton(uint16_t* array);


uint8_t KP_Heart[SNAKE_ELEMENTR_SIZE][SNAKE_ELEMENTR_SIZE] =
{
		{0,1,1,0,0,0,0,1,1,0},	//1
		{0,1,1,1,0,0,1,1,1,0},	//2
		{1,1,1,1,1,1,1,1,1,1},	//3
		{1,1,1,1,1,1,1,1,1,1},	//4
		{0,1,1,1,1,1,1,1,1,0},	//5
		{0,0,1,1,1,1,1,1,0,0},	//6
		{0,0,1,1,1,1,1,1,0,0},	//7
		{0,0,0,1,1,1,1,0,0,0},	//8
		{0,0,0,1,1,1,1,0,0,0},	//9
		{0,0,0,0,1,1,0,0,0,0}	//10
};

void InitScreen()
{
	BSP_LCD_Init();
	BSP_LCD_LayerDefaultInit(LCD_BACKGROUND_LAYER, LCD_FRAME_BUFFER);

	// UWAGA! inicjalizacja wyswietlacza lcd [funkcja: GLCD_Initialize()) musi byc pierwsza,
	// a funkcja inicjalizujaca dotyk (STMPE811_Init()) dopiero po niej!
	// ustawienie orientacji panelu dotykowego
	LcdTouchData.orientation = TM_STMPE811_Orientation_Portrait_2;
	/* Inicjalizacja obslugi ukladu odczytujacego zdarzenie i pozycję dotknięcia. */
	STMPE811_Init();
}
void KP_Snake_Start()
{
	// Inicjalizacja ekranu
	InitScreen();
	DrawGameMenu();
	while(1)
	{
		TM_STMPE811_ReadTouch(&LcdTouchData);
		if (LcdTouchData.pressed==TM_STMPE811_State_Pressed)
		{
			TouchScreenCoordinates[0] = LcdTouchData.x;
			TouchScreenCoordinates[1] = LcdTouchData.y;
			switch(CheckButton(TouchScreenCoordinates))
			{
			case GAME_MENU_BUTTON_1:
				RunNewGame();
				DrawGameMenu();
				break;
			case GAME_MENU_BUTTON_2:
				DrawGameInfo();
				DrawGameMenu();
				break;
			case GAME_MENU_BUTTON_3:
				DrawControlInfo();
				HAL_Delay(5000);
				DrawGameMenu();
				break;
			}
		}
		HAL_Delay(300);
	}
}
void DrawGameMenu()
{
	// Ustawienie koloru tła i wielkość czcionki
	BSP_LCD_Clear(BOARD_BCK_COLOR);
	BSP_LCD_SetFont(&Font20);
	// wyświetlenie przycisków menu
	BSP_LCD_SetBackColor(GAME_TEXT_INFO_COLOR);
	BSP_LCD_SetTextColor(GAME_TEXT_INFO_COLOR);
	BSP_LCD_FillRect(GAME_MENU_BUTTON_X, GAME_MENU_BUTTON_1_Y, GAME_MENU_BUTTON_WIDTH, GAME_MENU_BUTTON_HEIGHT);
	BSP_LCD_FillRect(GAME_MENU_BUTTON_X, GAME_MENU_BUTTON_2_Y, GAME_MENU_BUTTON_WIDTH, GAME_MENU_BUTTON_HEIGHT);
	BSP_LCD_FillRect(GAME_MENU_BUTTON_X, GAME_MENU_BUTTON_3_Y, GAME_MENU_BUTTON_WIDTH, GAME_MENU_BUTTON_HEIGHT);
	BSP_LCD_SetTextColor(BOARD_BCK_COLOR);
	BSP_LCD_DisplayStringAt(0, GAME_MENU_LINE_1_Y, (uint8_t*)"NEW GAME", CENTER_MODE);
	BSP_LCD_DisplayStringAt(0, GAME_MENU_LINE_2_Y, (uint8_t*)"GAME INFO", CENTER_MODE);
	BSP_LCD_DisplayStringAt(0, GAME_MENU_LINE_3_Y, (uint8_t*)"CONTROL", CENTER_MODE);
}
void RunNewGame()
{
	// odliczanie do rozpoczęcia nowej gry
	uint8_t end = 5;
	BSP_LCD_Clear(BOARD_BCK_COLOR);
	BSP_LCD_SetBackColor(BOARD_BCK_COLOR);
	while(end)
	{
		BSP_LCD_SetFont(&Font20);
		BSP_LCD_SetTextColor(GAME_TEXT_INFO_COLOR);
		BSP_LCD_DisplayStringAt(0, 140, (uint8_t*)"GAME START", CENTER_MODE);
		char buffer[20];
		snprintf(buffer, sizeof(buffer), "%d", end);
		BSP_LCD_DisplayStringAt(0, 180, (uint8_t*)buffer, CENTER_MODE);
		HAL_Delay(1000);
		BSP_LCD_SetTextColor(BOARD_BCK_COLOR);
		BSP_LCD_DisplayStringAt(0, 180, (uint8_t*)buffer, CENTER_MODE);
		end--;
	}
	//------------------------------------------------------------------------
	// ustawienia gry
	DelayTime = GAME_SPEED_DEFAULT;
	Lifes = GAME_LIFE_MAX;
	// Rysowanie planszy
	DrawGameBoard();
	// ustawienie wartości początkowych Snake'a
	SetupDefaultSnake();
	// wylosuj nowe jabłko
	GenerateNewTarget();
	while(Lifes)
	{
		// sczytanie koordynatów z ekranu dotykowego
		TM_STMPE811_ReadTouch(&LcdTouchData);
		if (LcdTouchData.pressed==TM_STMPE811_State_Pressed)
		{
			TouchScreenCoordinates[0] = LcdTouchData.x;
			TouchScreenCoordinates[1] = LcdTouchData.y;
			ScreenTouched = 1;
		}
		if(!GameOver)
		{
			if(ScreenTouched)
			{
				// zmiana kierunku jeśli jest możliwa
				switch(CheckDirection(TouchScreenCoordinates))
				{
				case SNAKE_DIRECTION_LEFT:
					if( snake.Direction != SNAKE_DIRECTION_RIGHT )
						snake.Direction = SNAKE_DIRECTION_LEFT;
					break;
				case SNAKE_DIRECTION_RIGHT:
					if( snake.Direction != SNAKE_DIRECTION_LEFT )
						snake.Direction = SNAKE_DIRECTION_RIGHT;
					break;
				case SNAKE_DIRECTION_UP:
					if( snake.Direction != SNAKE_DIRECTION_DOWN )
						snake.Direction = SNAKE_DIRECTION_UP;
					break;
				case SNAKE_DIRECTION_DOWN:
					if( snake.Direction != SNAKE_DIRECTION_UP )
						snake.Direction = SNAKE_DIRECTION_DOWN;
					break;
				}
				ScreenTouched = 0;
			}
			// pobranie i zapamietanie głowy snake'a
			Snake_Head[0] = Snake_Head_Last[0] = snake.Snake[snake.LastIndex][0];
			Snake_Head[1] = Snake_Head_Last[1] = snake.Snake[snake.LastIndex][1];
			// ruch snake'a
			switch(snake.Direction)
			{
			case SNAKE_DIRECTION_LEFT:
				Snake_Head[0] -= SNAKE_ELEMENTR_SIZE;
				break;
			case SNAKE_DIRECTION_RIGHT:
				Snake_Head[0] += SNAKE_ELEMENTR_SIZE;
				break;
			case SNAKE_DIRECTION_UP:
				Snake_Head[1] -= SNAKE_ELEMENTR_SIZE;
				break;
			case SNAKE_DIRECTION_DOWN:
				Snake_Head[1] += SNAKE_ELEMENTR_SIZE;
				break;
			default:
				break;
			}
			// sprawdzenie kolizji ze ścianą lub sobą
			if( Snake_Head[0] < BOARD_LEFT_X ||
				Snake_Head[0] > BOARD_RIGHT_X ||
				Snake_Head[1] < BOARD_LEFT_Y ||
				Snake_Head[1] > BOARD_RIGHT_Y ||
				MatchesSnakeLocations(Snake_Head))
				GameOver = 1;

			// sprawdzenie czy zebrano jabłko
			if( !GameOver &&
				Snake_Head[0] == Snake_Food[0] &&
				Snake_Head[1] == Snake_Food[1])
			{
				// dodaj nowy wpis do tablicy
				AddElementToSnake(Snake_Head_Last);
				DrawElement(Snake_Head_Last[0], Snake_Head_Last[1], 1);
				DrawElement(Snake_Head[0], Snake_Head[1], 1);
				//zwieksz liczbe punktow
				DrawGamePoints(snake.Points, 0);
				snake.Points += 1;
				if(snake.Points % 10 == 0)
					DelayTime -= 50;
				if(DelayTime < GAME_SPEED_MAX)
					DelayTime = GAME_SPEED_MAX;
				DrawGamePoints(snake.Points, 1);
				// wylosuj nowe jabłko
				GenerateNewTarget();
			}
			// rysowanie snake'a
			if(!GameOver)
			{
				// dodanie nowej głowy
				AddElementToSnake(Snake_Head);
				DeleteElementFromSnake(0, Snake_Foot);
				// wyczyszczenie zamazanie ogona
				DrawElement(Snake_Foot[0], Snake_Foot[1], 0);
				// narysowanie nowej głowy
				DrawElement(Snake_Head[0], Snake_Head[1], 1);
			}
		}

		if(GameOver)
		{
			// dodanie nowej głowy
			AddElementToSnake(Snake_Head_Last);
			// zmniejsz liczbę żyć
			Lifes--;
			if(Lifes)
			{
				char buffer[20];
				snprintf(buffer, sizeof(buffer), "%d", Lifes);
				// pokaż info
				DrawEndInfo("LIFE LEFT",buffer,1);
				HAL_Delay(1000);
				// ukryj info
				DrawEndInfo("LIFE LEFT",buffer,0);
				// nowy Snake
				SetupDefaultSnake();
				// Rysowanie planszy
				DrawGameBoard();
				// wylosuj nowe jabłko
				GenerateNewTarget();
			}
			else
			{
				// pokaż info
				DrawEndInfo("GAME","OVER",1);
				HAL_Delay(1000);
				// ukryj info
				DrawEndInfo("GAME","OVER",0);
			}
			// reset zmiennych
			GameOver = 0;
			DelayTime = GAME_SPEED_DEFAULT;
		}
		HAL_Delay(DelayTime);
	}
}
void DrawControlInfo()
{
	// Ustawienie koloru tła i wielkość czcionki
	BSP_LCD_Clear(BOARD_BCK_COLOR);
	BSP_LCD_SetFont(&Font24);
	// UP & DOWN
	BSP_LCD_SetBackColor(LCD_COLOR_LIGHTBLUE);
	BSP_LCD_SetTextColor(LCD_COLOR_LIGHTBLUE);
	BSP_LCD_FillRect(0,0,240,100);
	BSP_LCD_FillRect(0,220,240,100);
	BSP_LCD_SetTextColor(LCD_COLOR_BLACK);
	BSP_LCD_DisplayStringAt(0, 50, (uint8_t*)"UP", CENTER_MODE);
	BSP_LCD_DisplayStringAt(0, 260, (uint8_t*)"DOWN", CENTER_MODE);
	// LEFT
	BSP_LCD_SetBackColor(LCD_COLOR_LIGHTRED);
	BSP_LCD_SetTextColor(LCD_COLOR_LIGHTRED);
	BSP_LCD_FillRect(0,100,120,120);
	BSP_LCD_SetTextColor(LCD_COLOR_BLACK);
	BSP_LCD_DisplayStringAt(20, 160, (uint8_t*)"LEFT", LEFT_MODE);
	// RIGHT
	BSP_LCD_SetBackColor(LCD_COLOR_LIGHTGREEN);
	BSP_LCD_SetTextColor(LCD_COLOR_LIGHTGREEN);
	BSP_LCD_FillRect(120,100,120,120);
	BSP_LCD_SetTextColor(LCD_COLOR_BLACK);
	BSP_LCD_DisplayStringAt(140, 160, (uint8_t*)"RIGHT", LEFT_MODE);
}
void DrawGameInfo()
{
	// Ustawienie koloru tła i wielkość czcionki
	BSP_LCD_Clear(BOARD_BCK_COLOR);
	BSP_LCD_SetBackColor(BOARD_BCK_COLOR);
	BSP_LCD_SetFont(&Font24);
	BSP_LCD_SetTextColor(GAME_TEXT_INFO_COLOR);
	BSP_LCD_DisplayStringAt(0, 20, (uint8_t*)GAME, CENTER_MODE);
	BSP_LCD_SetFont(&Font16);
	BSP_LCD_DisplayStringAt(0, 45, (uint8_t*)VERSION, CENTER_MODE);
	BSP_LCD_SetFont(&Font20);
	BSP_LCD_DisplayStringAt(0, 65, (uint8_t*)AUTHOR, CENTER_MODE);
	BSP_LCD_SetFont(&Font12);
	uint16_t startLine = 100, space = 20, line = 0, end = 0;
	BSP_LCD_DisplayStringAt(5, startLine + space * line, (uint8_t*)"Projekt zaliczeniowy gra SNAKE.", LEFT_MODE);
	line++;
	BSP_LCD_DisplayStringAt(5, startLine + space * line, (uint8_t*)"Liczba zyc w grze wynosi 3 i sa", LEFT_MODE);
	line++;
	BSP_LCD_DisplayStringAt(5, startLine + space * line, (uint8_t*)"one wyswietlane obok punktow.", LEFT_MODE);
	line++;
	BSP_LCD_DisplayStringAt(5, startLine + space * line, (uint8_t*)"Utrata ostatniego zycia konczy", LEFT_MODE);
	line++;
	BSP_LCD_DisplayStringAt(5, startLine + space * line, (uint8_t*)"gre. Zmiana poziomu trudnosci", LEFT_MODE);
	line++;
	BSP_LCD_DisplayStringAt(5, startLine + space * line, (uint8_t*)"wzrasta po zebraniu 10 punktow.", LEFT_MODE);
	// przycisk powrotu
	BSP_LCD_SetFont(&Font20);
	BSP_LCD_SetBackColor(GAME_TEXT_INFO_COLOR);
	BSP_LCD_SetTextColor(GAME_TEXT_INFO_COLOR);
	BSP_LCD_FillRect(GAME_MENU_BUTTON_X, GAME_MENU_BUTTON_3_Y, GAME_MENU_BUTTON_WIDTH, GAME_MENU_BUTTON_HEIGHT);
	BSP_LCD_SetTextColor(BOARD_BCK_COLOR);
	BSP_LCD_DisplayStringAt(0, GAME_MENU_LINE_3_Y, (uint8_t*)"BACK", CENTER_MODE);
	while(!end)
	{
		TM_STMPE811_ReadTouch(&LcdTouchData);
		if (LcdTouchData.pressed==TM_STMPE811_State_Pressed)
		{
			TouchScreenCoordinates[0] = LcdTouchData.x;
			TouchScreenCoordinates[1] = LcdTouchData.y;
			if(CheckButton(TouchScreenCoordinates) == GAME_MENU_BUTTON_3)
				end = 1;
		}
	}
}
void DrawGameBoard()
{
	// Ustawienie koloru tła
	BSP_LCD_Clear(BOARD_BCK_COLOR);
	// Ustawienie koloru ramki
	BSP_LCD_SetTextColor(BORDER_COLOR);
	// Linia górna
	BSP_LCD_FillRect(BORDER_X, BORDER_Y, BORDER_LENGTH_X, BORDER_SIZE);
	// Linia prawa
	BSP_LCD_FillRect(BORDER_X_RIGHT, BORDER_Y, BORDER_SIZE, BORDER_LENGTH_Y);
	// Linia dolna
	BSP_LCD_FillRect(BORDER_X, BORDER_Y_DOWN, BORDER_LENGTH_X, BORDER_SIZE);
	// Linia lewa
	BSP_LCD_FillRect(BORDER_X, BORDER_Y, BORDER_SIZE, BORDER_LENGTH_Y);
	// narysowanie żyć
	switch(Lifes)
	{
	case 1:
		DrawHeart(GAME_TEXT_LIFE_X_3, GAME_TEXT_LINE_1_Y);
		//DrawElement(GAME_TEXT_LIFE_X_3, GAME_TEXT_LINE_1_Y, 2);
		break;
	case 2:
		//DrawElement(GAME_TEXT_LIFE_X_2, GAME_TEXT_LINE_1_Y, 2);
		//DrawElement(GAME_TEXT_LIFE_X_3, GAME_TEXT_LINE_1_Y, 2);
		DrawHeart(GAME_TEXT_LIFE_X_2, GAME_TEXT_LINE_1_Y);
		DrawHeart(GAME_TEXT_LIFE_X_3, GAME_TEXT_LINE_1_Y);
		break;
	case 3:
		//DrawElement(GAME_TEXT_LIFE_X_1, GAME_TEXT_LINE_1_Y, 2);
		//DrawElement(GAME_TEXT_LIFE_X_2, GAME_TEXT_LINE_1_Y, 2);
		//DrawElement(GAME_TEXT_LIFE_X_3, GAME_TEXT_LINE_1_Y, 2);
		DrawHeart(GAME_TEXT_LIFE_X_1, GAME_TEXT_LINE_1_Y);
		DrawHeart(GAME_TEXT_LIFE_X_2, GAME_TEXT_LINE_1_Y);
		DrawHeart(GAME_TEXT_LIFE_X_3, GAME_TEXT_LINE_1_Y);
		break;
	}
	// Wypisanie tekstu
	BSP_LCD_SetBackColor(BOARD_BCK_COLOR);
	BSP_LCD_SetTextColor(GAME_TEXT_INFO_COLOR);
	BSP_LCD_SetFont(&Font16);
	BSP_LCD_DisplayStringAt(GAME_TEXT_LINE_1_X, GAME_TEXT_LINE_1_Y, (uint8_t*)"POINT:", LEFT_MODE);
	DrawGamePoints(0, 1);
	BSP_LCD_DisplayStringAt(0, 300, (uint8_t*)AUTHOR, CENTER_MODE);
}
void DrawGamePoints(uint16_t points, uint value)
{
	// Wypisanie liczby
	uint color;
	if(value == 0)
		color = BOARD_BCK_COLOR;
	else
		color = GAME_TEXT_INFO_COLOR;
	char buffer[20];
	snprintf(buffer, sizeof(buffer), "%d", points);
	BSP_LCD_SetTextColor(color);
	BSP_LCD_DisplayStringAt(GAME_TEXT_POINTS_X, GAME_TEXT_LINE_1_Y, (uint8_t*)buffer, LEFT_MODE);
}
void DrawElement(uint16_t x, uint16_t y, uint value)
{
	uint color;
	// ustawienie koloru
	switch(value)
	{
	case 0:
		color = BOARD_BCK_COLOR;	// tło
		break;
	case 1:
		color = SNAKE_COLOR_PIXEL;	// snake
		break;
	case 2:
		color = SNAKE_COLOR_APPLE;	// jabłko
		break;
	}

	BSP_LCD_SetTextColor(color);
	BSP_LCD_FillRect(x, y, SNAKE_ELEMENTR_SIZE, SNAKE_ELEMENTR_SIZE);
}
void DrawHeart(uint16_t x, uint16_t y)
{
	for(uint8_t i = 0; i < SNAKE_ELEMENTR_SIZE; ++i)
	{
		for(uint8_t j = 0; j < SNAKE_ELEMENTR_SIZE; ++j)
		{
			if(KP_Heart[j][i] == 1)
				BSP_LCD_DrawPixel(x + i, y + j, SNAKE_COLOR_APPLE);
		}
	}
}
void DrawEndInfo(char* text1, char* text2, uint value)
{
	uint color;
	if(value == 0)
		color = BOARD_BCK_COLOR;
	else
		color = GAME_TEXT_INFO_COLOR;
	BSP_LCD_SetTextColor(color); // Ustawienie koloru na "zielony"
	BSP_LCD_SetFont(&Font16); // Ustawienie rozmiaru czcionki
	BSP_LCD_DisplayStringAt(0, GAME_TEXT_LINE_2_Y, (uint8_t *)text1, CENTER_MODE); // Wypisanie tekstu
	BSP_LCD_DisplayStringAt(0, GEME_TEXT_LINE_3_Y, (uint8_t *)text2, CENTER_MODE); // Wypisanie tekstu
}
void SetupDefaultSnake()
{
	snake.Direction = SNAKE_DIRECTION_UP;
	snake.LastIndex = 0;
	snake.Length = 0;
	snake.Points = 0;

	Snake_Head[0] = 110;
	Snake_Head[1] = 180;

	AddElementToSnake(Snake_Head);
	DrawElement(Snake_Head[0], Snake_Head[1], 1);
}
void AddElementToSnake(uint16_t* array)
{
	// ustawienie indeksów
	if (snake.Length == 0)
		snake.Length++;
	else
	{
		snake.LastIndex++;
		snake.Length++;
	}

	// dodanie punktu do tablicy Snake'a
	snake.Snake[snake.LastIndex][0] = array[0];
	snake.Snake[snake.LastIndex][1] = array[1];
}
void DeleteElementFromSnake(uint16_t index, uint16_t* array)
{
	if(index < snake.Length)
	{
		// zapisanie danych do usuniecia
		array[0] = snake.Snake[index][0];
		array[1] = snake.Snake[index][1];

		// przepisanie tablicy
		for(;index < snake.LastIndex; ++index)
		{
			snake.Snake[index][0] = snake.Snake[index + 1][0];
			snake.Snake[index][1] = snake.Snake[index + 1][1];
		}
		if(snake.LastIndex != 0)
		{
			snake.LastIndex--;
			snake.Length--;
		}
		else
		{
			snake.LastIndex = 0;
			snake.Length = 1;
		}
	}
}
void GenerateNewTarget()
{
	do
	{
		// wygeneruj koordynaty jabłka
		GenerateRandomCoordinates(Snake_Food);
	}while(MatchesSnakeLocations(Snake_Food));

	DrawElement(Snake_Food[0], Snake_Food[1], 2);
}
void GenerateRandomCoordinates(uint16_t* array)
{
	array[0] = rand() % ((BOARD_RIGHT_X - BOARD_LEFT_X) / SNAKE_ELEMENTR_SIZE + 1) * SNAKE_ELEMENTR_SIZE + BOARD_LEFT_X;
	array[1] = rand() % ((BOARD_RIGHT_Y - BOARD_LEFT_Y) / SNAKE_ELEMENTR_SIZE + 1) * SNAKE_ELEMENTR_SIZE + BOARD_LEFT_Y;
}
uint8_t MatchesSnakeLocations(uint16_t* array)
{
	for(uint16_t i = 0; i < snake.Length; ++i)
	{
		if( snake.Snake[i][0] == array[0] &&
			snake.Snake[i][1] == array[1])
		{
			// kolizja
			return 1;
		}
	}
	// brak kolizji
	return 0;
}
uint16_t CheckDirection(uint16_t* array)
{
	if(array[1] > 0 && array[1] < 100)
		return SNAKE_DIRECTION_DOWN;

	if(array[1] > 220 && array[1] < 320)
		return SNAKE_DIRECTION_UP;

	if(array[0] > 0 && array[0] < 120)
		return SNAKE_DIRECTION_LEFT;

	else
		return SNAKE_DIRECTION_RIGHT;
}
uint16_t CheckButton(uint16_t* array)
{
	if(array[0] >= GAME_MENU_BUTTON_X && array[0] <= GAME_MENU_BUTTON_X + GAME_MENU_BUTTON_WIDTH)
	{
		if(array[1] >= (320 -(GAME_MENU_BUTTON_1_Y + GAME_MENU_BUTTON_HEIGHT)) && array[1] <= (320 - GAME_MENU_BUTTON_1_Y))
			return GAME_MENU_BUTTON_1;
		if(array[1] >= (320 -(GAME_MENU_BUTTON_2_Y + GAME_MENU_BUTTON_HEIGHT)) && array[1] <= (320 - GAME_MENU_BUTTON_2_Y))
			return GAME_MENU_BUTTON_2;
		if(array[1] >= (320 -(GAME_MENU_BUTTON_3_Y + GAME_MENU_BUTTON_HEIGHT)) && array[1] <= (320 - GAME_MENU_BUTTON_3_Y))
			return GAME_MENU_BUTTON_3;
	}
	return 0;
}
