/**	
 * |----------------------------------------------------------------------
 * | Copyright (C) Tilen Majerle, 2014
 * | 
 * | This program is free software: you can redistribute it and/or modify
 * | it under the terms of the GNU General Public License as published by
 * | the Free Software Foundation, either version 3 of the License, or
 * | any later version.
 * |  
 * | This program is distributed in the hope that it will be useful,
 * | but WITHOUT ANY WARRANTY; without even the implied warranty of
 * | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * | GNU General Public License for more details.
 * | 
 * | You should have received a copy of the GNU General Public License
 * | along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * |----------------------------------------------------------------------
 */
#include "i2c_stm32_my.h"
#include <inttypes.h>
 

#define TIMEOUT  20000
uint16_t timeout_G;

/* Private defines */
#define I2C_TRANSMITTER_MODE   0
#define I2C_RECEIVER_MODE      1
#define I2C_ACK_ENABLE         1
#define I2C_ACK_DISABLE        0
 
/***************************************************************************/
void I2C_Init (void)
{
	int time;
	
		// zmiana taktowania
 	RCC->CFGR				|= RCC_CFGR_PPRE1_DIV4; // APB2 = 90MHz/4 = 45MHz
	
	// wlaczenie taktowania dla I2C3
	RCC->APB1ENR	|= RCC_APB1ENR_I2C3EN;
	
	// funkcje alternatywne pinow PA8 i PC9
	GPIOA->MODER |= GPIO_MODER_MODER8_1;
	GPIOC->MODER |= GPIO_MODER_MODER9_1;
	
	//wybor funkcji alternatywnej dla danych pinow
	GPIOA->AFR[1] |= 0x00000004; 
	GPIOC->AFR[1] |= 0x00000040;
	
	// Otwarty kolektor
	GPIOA->OTYPER |= GPIO_OTYPER_OT_8;
	GPIOC->OTYPER |= GPIO_OTYPER_OT_9;
	
	// Najwyzsza predkosc
	GPIOA->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR8;
	GPIOC->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR9;
	
	// reset  modulu I2C3
	RCC->APB1RSTR |= RCC_APB1RSTR_I2C3RST; 
	RCC->APB1RSTR &= (~RCC_APB1RSTR_I2C3RST); 
	
	// ustawienie czestotliwosci taktowania  I2C3
	I2C3->CR2 |=  I2C_CR2_FREQ_0 | I2C_CR2_FREQ_2 | I2C_CR2_FREQ_3 | I2C_CR2_FREQ_5;                  

	// Master Mode Selection:  tryb FM
  I2C3->CCR |= I2C_CCR_FS;
	
			// reset programowy
	I2C3->CR1 |= I2C_CR1_SWRST;
	for(time=0; time<=500000; time++) {}; // chwila pauzy
	I2C3->CR1 &= (~I2C_CR1_SWRST);
	
	// bez rozciagania SCL
	//I2C3->CR1 |= I2C_CR1_NOSTRETCH; 
	
	// 1: Fm mode tlow/thigh = 16/9 (see CCR)
	// 0: Fm mode tlow/thigh = 2
	I2C3->CCR &= ~I2C_CCR_DUTY;	
	
	I2C3->OAR1 |= 0x00;
 
	I2C3-> CCR = 0x0060;	// Czestotliwosc    kHz
	I2C3-> TRISE = 0x003F;	// Max czas narastania w trybie Fm/Sm  (Master mode)  

	I2C3->FLTR &= ~I2C_FLTR_ANOFF; // analogowy filtr przeciwzakloceniowy na linii I2C		
	
	I2C3->CR1 |= I2C_CR1_PE;	// wlacz urzadzenia peryferyjne
}

/***************************************************************************/
void I2C_WriteData(uint8_t data) 
{
	// Poczekaj, az I2C nie bedzie zajety
	timeout_G = TIMEOUT;
	// I2C_SR1_TXE - Data Register Empty (transmitters)
	while (!(I2C3->SR1 & I2C_SR1_TXE) && timeout_G) 
	{
		timeout_G--;
	}
	
	// wyslij dane
	I2C3->DR = data;
}

/***************************************************************************/
uint8_t I2C_Stop(void) 
{
	// Poczekaj, az nadajnik nie bedzie pusty
	timeout_G = TIMEOUT;
	while (((!(I2C3->SR1 & I2C_SR1_TXE)) || (!(I2C3->SR1 & I2C_SR1_BTF)))) 
	{ 
		// I2C_SR1_TXE - Data Register Empty (transmitters)
		// I2C_SR1_BTF - Byte Transfer Finished  
		if (--timeout_G == 0x00) 
		{
			return 1;
		}
	}
	
	// Generowanie stopu
	I2C3->CR1 |= I2C_CR1_STOP;
	
	// zwraca 0, wszystko okej
	return 0;
}

/***************************************************************************/ 
int16_t I2C_Start(uint8_t adres, uint8_t tryb_pracy, uint8_t bit_ack)
{
	#define ERROR_START		1
	
	// generowanie impulsu startowego
	I2C3->CR1 |= I2C_CR1_START;
	
	// oczekiwanie na odpowiedz
	timeout_G = TIMEOUT;
	while(!(I2C3->SR1 & I2C_SR1_SB))
	{
		if(--timeout_G == 0x00)
		{
			return ERROR_START;
		};
	};
	
	// wlaczenie ACK
	if (bit_ack) 
	{
		I2C3->CR1 |= I2C_CR1_ACK;
	};
	
	// nadawanie
	if (tryb_pracy == I2C_TRANSMITTER_MODE) 
	{
		// Wyslij adres z ostatnim bitem 
		I2C3->DR = adres & ~I2C_OAR1_ADD0;
			
		// Oczekiwanie, az skonczy 
		timeout_G = TIMEOUT;
		while (!(I2C3->SR1 & I2C_SR1_ADDR)) 
		{
			if (--timeout_G == 0x00) 
			{
				return ERROR_START;
			}
		}
	}
		// odbieranie
	if (tryb_pracy == I2C_RECEIVER_MODE) 
	{	
		// Wyslij adres z ostatnim bitem
		I2C3->DR = adres | I2C_OAR1_ADD0;
		
		// Oczekiwanie, az skonczy 
		timeout_G = TIMEOUT;
		
		while (!(I2C3->SR1 & I2C_SR1_ADDR)) 
		{
			if (--timeout_G == 0x00) 
			{
				return ERROR_START;
			}
		}
	}
	
	// Odczyt rejestr statusu, aby wyczyscic flage ADDR 
	I2C3->SR2;
	
	// Zwroc 0 jesli wszystko ok
	return 0;
	
};


/***************************************************************************/
uint8_t I2C_ReadAck(void) 
{
	uint8_t data;
	
	// Wlacz ACK
	I2C3->CR1 |= I2C_CR1_ACK;
	
	// Oczekiwanie na odebranie danych
	timeout_G = TIMEOUT;
	while (!(I2C3->SR1 & I2C_SR1_RXNE)) // nie wiem czy dobrze
	{
		if (--timeout_G == 0x00) 
		{
			return 1; // zwroc blad
		}
	}
	
	// Odczyt danych
	data = I2C3->DR;
	
	// Zwroc dane
	return data;
}

/***************************************************************************/
uint8_t I2C_ReadNack(void) 
{
	uint8_t data;
	
	// Wylaczenie ACK  
	I2C3->CR1 &= ~I2C_CR1_ACK;
	
	// Generowanie stopu 
	I2C3->CR1 |= I2C_CR1_STOP;
	
	// Czekaj, az odbierze
	timeout_G = TIMEOUT;
	while (!(I2C3->SR1 & I2C_SR1_RXNE)) // nie wiem czy dobrze
	{
		if (--timeout_G == 0x00) 
		{
			// zwroc blad
			return 1;
		}
	}

	// Odczyt danych z rejestru DR
	data = I2C3->DR;
	
	// Zwroc dane
	return data;
}

/***************************************************************************/
void I2C_ReadMulti( uint8_t address, uint8_t reg, uint8_t* data, uint16_t count) 
{
	I2C_Start(address, I2C_TRANSMITTER_MODE, I2C_ACK_ENABLE);
	I2C_WriteData(reg);
	I2C_Start(address, I2C_RECEIVER_MODE, I2C_ACK_ENABLE);
	while (count--) 
	{
		if (!count) 
		{
			// Ostatni bajt
			*data++ = I2C_ReadNack();
		} else {
			*data++ = I2C_ReadAck();
		}
	}
}

/***************************************************************************/
uint8_t I2C_Read(uint8_t address, uint8_t reg) 
{
	uint8_t received_data;
	I2C_Start(address, I2C_TRANSMITTER_MODE, I2C_ACK_DISABLE);
	I2C_WriteData(reg);
	I2C_Stop();
	I2C_Start(address, I2C_RECEIVER_MODE, I2C_ACK_DISABLE);
	received_data = I2C_ReadNack();
	return received_data;
}

/***************************************************************************/
void I2C_Write(uint8_t address, uint8_t reg, uint8_t data) 
{
	I2C_Start(address, I2C_TRANSMITTER_MODE, I2C_ACK_DISABLE);
	I2C_WriteData(reg);
	I2C_WriteData(data);
	I2C_Stop();
};
/***************************************************************************/

 
